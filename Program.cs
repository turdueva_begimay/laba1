using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Threading;

namespace NotebookApp
{
    class Notebook
    {
        static void Main()
        {
            List<Note> allNotes = new List<Note>();
            StartWindow();
            while (true)
            {
                ShowStartMenu();
                int answer = int.Parse(Console.ReadLine());
                switch (answer)
                {
                    case 1:
                        CreateNewNote(allNotes);
                        Console.WriteLine("Запись успешно создана!");
                        Console.ReadKey();
                        Console.Clear();
                        break;

                    case 2:
                        Console.WriteLine("Выберите запись для редактирования");
                        ShowAllNotes(allNotes);
                        int ansNote = int.Parse(Console.ReadLine());
                        Console.WriteLine("Выберите поле, которое хотите отредактировать:");
                        Console.WriteLine("[1 - Фамилия       ]");
                        Console.WriteLine("[2 - Имя           ]");
                        Console.WriteLine("[3 - Отчество      ]");
                        Console.WriteLine("[4 - Номер телефона]");
                        Console.WriteLine("[5 - Страна        ]");
                        Console.WriteLine("[6 - Организация   ]");
                        Console.WriteLine("[7 - Должность     ]");
                        Console.WriteLine("[8 - Прочие заметки]");
                        Console.WriteLine();
                        int fieldN = int.Parse(Console.ReadLine());
                        Console.WriteLine("Введите отредактированный текст: ");
                        string newNote = Console.ReadLine();
                        EditNotes(ansNote, fieldN, allNotes, newNote);
                        Console.WriteLine("Запись успешно отредактирована!");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 3:
                        ShowAllNotes(allNotes);
                        Console.WriteLine("Выберите запись, которую хотите удалить!");
                        int ansDelete = int.Parse(Console.ReadLine());
                        DeleteNote(ansDelete, allNotes);
                        Console.WriteLine("Запись успешно удалена!");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 4:
                        ShowAllNotes(allNotes);
                        Console.WriteLine("Выберите запись, которую хотите полностью просмотреть!");
                        int ansShow = int.Parse(Console.ReadLine());
                        ShowFullInfoNote(ansShow, allNotes);
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 5:
                        ShowAllNotes(allNotes);
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 6:
                        Console.WriteLine("Вы действительно хотите выйти из программы?(Введите \"да/нет\")");
                        string ans_exit = Console.ReadLine();
                        if (ans_exit.Equals("да"))
                        {
                            Console.WriteLine("До встречи!");
                        }
                        break;

                }
            }
        }

        //начальное окно приветствия
        public static void StartWindow()
        {
            Console.WriteLine("Добро пожаловать в мини-программу \"Notebook.Lite\", версия 1.0!");
            Console.WriteLine("             Выберите нужную опцию из панели меню! ");
            Console.WriteLine("----------------------------------------------------------------");
        }

        //окно меню
        static void ShowStartMenu()
        {
            Console.WriteLine("< -----------------Меню----------------->");
            Console.WriteLine("[     1 - Создание новой записи         ]");
            Console.WriteLine("[     2 - Редактирование записи         ]");
            Console.WriteLine("[     3 - Удаление записи               ]");
            Console.WriteLine("[     4 - Просмотр записи               ]");
            Console.WriteLine("[     5 - Просмотр всех записей         ]");
            Console.WriteLine("[     6 - Выход из программы            ]");
            Console.WriteLine();
        }

        //Создание новой записи
        public static void CreateNewNote(List<Note> allNotes)
        {
            Note note = new Note();
            do
            {
                Console.Write("Фамилия: ");
                note.LastName = Console.ReadLine();
                if (string.IsNullOrEmpty(note.LastName))
                {
                    Console.WriteLine("Поле обязательное для ввода!");
                }
            } while (string.IsNullOrEmpty(note.LastName));

            do
            {
                Console.Write("Имя: ");
                note.FirstName = Console.ReadLine();
                if (string.IsNullOrEmpty(note.FirstName))
                {
                    Console.WriteLine("Поле обязательное для ввода!");
                }
            } while (string.IsNullOrEmpty(note.FirstName));

            Console.Write("Отчество: ");
            note.MiddleName = Console.ReadLine();
            try
            {
                Console.Write("Номер телефона: ");
                note.NumberPhone = int.Parse(Console.ReadLine());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Некорректный ввод данных!"); ;
            }

            try
            {
                Console.Write("Дата рождения (в формате дд/мм/гггг): ");
                note.DateOfBirth = DateTime.Parse(Console.ReadLine());
                do
                {
                    Console.Write("Страна: ");
                    note.Country = Console.ReadLine();
                    if (Enum.IsDefined(typeof(Countries), note.Country) != true) { Console.WriteLine("Данной страны в списке нет"); }
                } while (string.IsNullOrEmpty(note.Country));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Неправильный формат данных");
            }


            Console.Write("Организация: ");
            note.OrganizationName = Console.ReadLine();
            Console.Write("Должность: ");
            note.Position = Console.ReadLine();
            Console.Write("Прочие заметки: ");
            note.OtherNotes = Console.ReadLine();
            allNotes.Add(note);
        }

        //удаление записи
        public static void DeleteNote(int id, List<Note> allNotes)
        {
            for (int i = 0; i < allNotes.Count + 1; i++)
            {
                if ((id - 1) == i)
                    allNotes.RemoveAt(i);
            }
        }

        //просмотр всех записей
        public static void ShowAllNotes(List<Note> allNotes)
        {
            for (int i = 0; i < allNotes.Count; i++)
            {
                Console.WriteLine(allNotes[i].GetBriefInfo(i + 1));
            }
        }

        //просмотр записи c отображением полной информации 
        public static void ShowFullInfoNote(int id, List<Note> allNotes)
        {
            for (int i = 0; i < allNotes.Count; i++)
            {
                if ((id - 1) == i)
                {
                    Console.WriteLine(allNotes[i].GetFullInfo(i));
                }
            }
        }

        //редактирование полей
        public static void EditNotes(int id, int numField, List<Note> allNotes, string newStr)
        {
            for (int i = 0; i < allNotes.Count; i++)
            {
                if ((id - 1) == i)
                {
                    allNotes[i].EditDefiniteField(numField, newStr);
                }
            }
        }

    }
}
