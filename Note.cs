﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookApp
{
    enum Countries
    {
        Россия, Америка, Казахстан, Китай, Индия
    }
    class Note
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public int NumberPhone { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Country { get; set; }
        public string OrganizationName { get; set; }
        public string Position { get; set; }
        public string OtherNotes { get; set; }

        //просмотра всех созданных учетных записей, с краткой информацией.
        public string GetBriefInfo(int id)
        {
            return ("Запись #" + id + " Фамилия: " + this.LastName + " Имя: " + this.FirstName + " Номер телефона:" + this.NumberPhone);
        }

        //просмотр созданных учетных записей
        public string GetFullInfo(int id)
        {
            return ("Запись #" + id + "\nФамилия: " + this.LastName + "\nИмя: " + this.FirstName + "\nНомер телефона:" + this.NumberPhone + "\nДата Рождения:" + this.DateOfBirth + "\nСтрана:" + this.Country + "\nОрганизация: " + this.OrganizationName + "Должность: " + this.Position + "\nПрочие заметки: " + this.OtherNotes);
        }

        //метод позволяет отредактировать определенное поле 
        public void EditDefiniteField(int nfield, string newStr)
        {
            switch (nfield)
            {
                case 1:
                    LastName = newStr;
                    break;
                case 2:
                    FirstName = newStr;
                    break;
                case 3:
                    MiddleName = newStr;
                    break;
                case 4:
                    NumberPhone = int.Parse(newStr);
                    break;
                case 5:
                    Country = newStr;
                    break;
                case 6:
                    DateOfBirth = DateTime.Parse(newStr);
                    break;
                case 7:
                    OrganizationName = newStr;

                    break;
                case 8:
                    Position = newStr;
                    break;
                case 9:
                    OtherNotes = newStr;
                    break;
            }

        }
    }
}
